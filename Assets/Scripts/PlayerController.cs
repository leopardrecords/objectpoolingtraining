using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private GameObject projectile;
    [SerializeField] private Transform firePosition;

    private float moveDirection;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        moveDirection = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(moveDirection * moveSpeed, 0);

        if (Input.GetMouseButton(0))
        {
            Fire();
        }
    }

    private void Fire()
    {
        GameObject obj = ObjectPooler.current.GetPooledObject();
        if (obj == null)
            return;

        obj.transform.position = firePosition.position;
        obj.transform.rotation = firePosition.rotation;
        obj.SetActive(true);
    }
}
