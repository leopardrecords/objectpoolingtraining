using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    [SerializeField] private float projectileSpeed;
    [SerializeField] private float disableDelay = 1f;

    private Rigidbody2D rb;

    private void OnEnable()
    {
        if (rb != null)
        {
            rb.velocity = Vector2.up * projectileSpeed;
        }
        Invoke("Disable", disableDelay);
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.up * projectileSpeed;
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }
}
